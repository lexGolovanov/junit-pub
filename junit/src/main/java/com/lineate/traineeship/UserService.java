package com.lineate.traineeship;

import java.util.Collection;

public interface UserService {
    User createUser(String name, Group group);

    Group createGroup(String name, Collection<Permission> permissions);
}
