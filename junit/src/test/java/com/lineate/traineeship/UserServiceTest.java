package com.lineate.traineeship;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.assertj.core.api.Assertions;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

// @TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("Tests of User operations")
class UserServiceTest {
    private static UserService userService;

    @BeforeAll
    static void initUserService() {
        final ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
    }

    @Test
    void testCreateUser() {
        final String userName = "AlexeyPryadko";
        final String groupName = "DefaultGroup";
        final List<Permission> groupPermissions = new ArrayList<>();
        Collections.addAll(groupPermissions, Permission.read);

        final Group group = userService.createGroup(groupName, groupPermissions);
        final User user = userService.createUser(userName, group);
        group.addUser(user);

        Assertions.assertThat(user)
                .isNotNull();
        Assertions.assertThat(user)
                .extracting(User::getName)
                .isEqualTo(userName);
        Assertions.assertThat(user)
                .extracting(User::getGroups)
                .isEqualTo(Collections.singletonList(group));
    }

    Stream<Arguments> wrongUserParams() {
        final Group group = userService.createGroup(
                "UserName",
                Collections.singletonList(Permission.read)
        );

        return Stream.of(
                Arguments.arguments(null, group),
                Arguments.arguments("UserName_OK", null)
        );
    }

    @ParameterizedTest
    @MethodSource("wrongUserParams")
    void testCreateUserFailed(String userName, Group group) {
        final User user  = userService.createUser(userName, group);
        assertNull(user);
    }
}