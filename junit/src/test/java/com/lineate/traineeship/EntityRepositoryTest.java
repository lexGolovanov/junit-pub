package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EntityRepositoryTest {
    private static EntityRepository mockRepository;
    private static Entity mockEntity;
    private static UserService userService;
    private static EntityService entityService;
    private static User user;
    private static Group group;

    @BeforeAll
    static void init() {
        mockRepository = mock(EntityRepository.class);
        mockEntity = mock(Entity.class);

        final ServiceFactory factory = new ServiceFactory();
        userService = factory.createUserService();
        entityService = factory.createEntityService(mockRepository);

        final String userName = "Richard";
        final String groupName = "RichardGroup";
        final List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        group = userService.createGroup(groupName, permissions);
        user = userService.createUser(userName, group);
        group.addUser(user);
    }

    @Test
    void testCreateEntity() {
        final String name = "Nelly_Furtado";
        final String value = "Showtime";

        doNothing()
                .when(mockRepository)
                .save(any(Entity.class));
        entityService.createEntity(user, name, value);

        final ArgumentCaptor<Entity> entityCaptor = ArgumentCaptor.forClass(Entity.class);
        verify(mockRepository, times(1))
                .save(entityCaptor.capture());

        final Entity capturedEntity = entityCaptor.getValue();
        assertNotNull(capturedEntity);
        assertEquals(name, capturedEntity.getName());
        assertEquals(value, capturedEntity.getValue());
    }

    @Test
    void testGetEntity() {
        final String name = "Fleetwood_Mac";
        final String value = "The_Chain";

        when(mockEntity.getName()).thenReturn(name);
        when(mockEntity.getValue()).thenReturn(value);
        when(mockEntity.getOwner()).thenReturn(user);
        when(mockEntity.getGroups()).thenReturn(user.getGroups());

        when(mockRepository.get(name))
                .thenReturn(mockEntity);
        final String foundValue = entityService.getEntityValue(user, name);

        assertEquals(value, foundValue);
        verify(mockRepository, times(1))
                .get(name);
    }

    @Test
    void testUpdateEntity() {
        final String name = "Lionel_Richie";
        final String value = "Running_With_the_Night";
        final String updValue = "Round_and_Round";

        doNothing()
                .when(mockRepository)
                .save(any(Entity.class));
        doNothing()
                .when(mockRepository)
                .save(any(Entity.class));

        entityService.createEntity(user, name, value);
        entityService.updateEntity(user, name, updValue);

        final ArgumentCaptor<Entity> entityCaptor = ArgumentCaptor.forClass(Entity.class);
        verify(mockRepository, times(2))
                .save(entityCaptor.capture());

        final List<Entity> capturedEntities = entityCaptor.getAllValues();
        assertAll(
                () -> assertEquals(name, capturedEntities.get(0).getName()),
                () -> assertEquals(name, capturedEntities.get(1).getName()),
                () -> assertEquals(value, capturedEntities.get(0).getValue()),
                () -> assertEquals(updValue, capturedEntities.get(1).getValue())
        );
    }
}
