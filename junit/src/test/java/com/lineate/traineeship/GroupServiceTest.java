package com.lineate.traineeship;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Tests for Group interface")
public class GroupServiceTest {
    private static UserService userService;

    @BeforeAll
    static void initUserService() {
        final ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
    }

    @Test
    void testCreateGroup() {
        final String groupName = "Group#1";
        final List<Permission> permissions = new ArrayList<>();
        Collections.addAll(permissions, Permission.read);

        final Group group = userService.createGroup(groupName, permissions);

        assertNotNull(group);
        assertAll("Group created",
                () -> assertEquals(groupName, group.getName()),
                () -> assertEquals(Collections.emptyList(), group.getUsers()),
                () -> assertEquals(0, group.getUsers().size()),
                () -> assertEquals(permissions, group.getPermissions())
        );
    }

    @Test
    @DisplayName("Group wouldn't be created because it should have name")
    void testCreateGroupWithNullGroupName() {
        final List<Permission> permissions = new ArrayList<>();
        Collections.addAll(permissions, Permission.read, Permission.write);

        final Group group = userService.createGroup(null, permissions);
        assertNull(group);
    }

    @Test
    @DisplayName("Group wouldn't be created because it should have list of permissions")
    void testCreateGroupWithNullPermissions() {
        final String groupName = "GroupName#1";
        final Group group = userService.createGroup(groupName, null);
        assertNull(group);
    }

    Stream<Arguments> wrongGroupParams() {
        return Stream.of(
                Arguments.arguments(null, Arrays.asList(Permission.read, Permission.write)),
                Arguments.arguments("GroupName_OK", null)
        );
    }

    @ParameterizedTest
    @MethodSource("wrongGroupParams")
    void testCreateGroupFailed(String groupName, List<Permission> groupPermissions) {
        final Group group = userService.createGroup(groupName, groupPermissions);
        assertNull(group);
    }

    @Test
    void testAddNewUserToGroup() {
        final String groupName = "DefaultGroup";
        final List<Permission> groupPermissions = new ArrayList<>();
        Collections.addAll(groupPermissions, Permission.read, Permission.write);
        final Group group = userService.createGroup(groupName, groupPermissions);

        final String userName1 = "AlexeyPryadko";
        final User user1 = userService.createUser(userName1, group);
        group.addUser(user1);
        assertIterableEquals(Collections.singletonList(user1), group.getUsers());

        final String userName2 = "IgorLink";
        final User user2 = userService.createUser(userName2, group);
        group.addUser(user2);
        assertIterableEquals(Arrays.asList(user1, user2), group.getUsers());
    }
}
