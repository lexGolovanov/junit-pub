package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class EntityServiceTest {
    private static UserService userService;
    private static EntityService entityService;
    private static User user;
    private static Group group;

    @BeforeAll
    static void initServices() {
        final ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
        entityService = serviceFactory.createEntityService();

        final String userName = "Richard";
        final String groupName = "RichardGroup";
        final List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        group = userService.createGroup(groupName, permissions);
        user = userService.createUser(userName, group);
        group.addUser(user);
    }

    @Test
    void testCreateEntity() {
        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        final boolean entityCreated = entityService.createEntity(user, entityName, entityValue);
        assertTrue(entityCreated);
    }

    Stream<Arguments> wrongEntityParams() {
        final String entityValue = "EntityValue";
        return Stream.of(
                Arguments.arguments(user, "Wrong Name With Spaces", entityValue),
                Arguments.arguments(user, "WrongWrongWrongWrongWrongWrongWrong", entityValue),
                Arguments.arguments(user, "", entityValue),
                Arguments.arguments(user, null, entityValue),
                Arguments.arguments(null, "OkName", "PogChamp")
        );
    }

    @ParameterizedTest
    @MethodSource("wrongEntityParams")
    void testCreateEntity_wrongParams_entityNotCreated(User user, String entityName, String entityValue) {
        final boolean areCreated = entityService.createEntity(user, entityName, entityValue);
        assertFalse(areCreated);
    }

    @Test
    void getEntityValueByEntityName() {
        final String entityName = "Name";
        final String entityValue = "Wert";
        entityService.createEntity(user, entityName, entityValue);

        final String foundValue = entityService.getEntityValue(user, entityName);
        assertEquals(entityValue, foundValue);
    }

    @Test
    @DisplayName("Not found entity value with wrong entity name")
    void testGetEntityValue_wrongEntityName_notReceivedEntityValue() {
        final String wrongEntityName = "WrongName";
        final String entityName = "OkName";
        final String entityValue = "OkValue";
        entityService.createEntity(user, entityName, entityValue);

        final String foundValue = entityService.getEntityValue(user, wrongEntityName);
        assertNotEquals(entityValue, foundValue);
    }

    @Test
    @DisplayName("Not found entity value from another user (he not in entity creator's groups)")
    void testGetEntityValue_wrongUser_notReceivedEntityValue() {
        final String userName = "AnotherUser";
        final String groupName = "AnotherGroup";
        final List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        final Group anotherGroup = userService.createGroup(groupName, permissions);
        final User anotherUser = userService.createUser(userName, anotherGroup);
        anotherGroup.addUser(anotherUser);

        final String entityName = "OkName";
        final String entityValue = "OkValue";
        entityService.createEntity(user, entityName, entityValue);

        final String foundValue = entityService.getEntityValue(anotherUser, entityName);
        assertNotEquals(entityValue, foundValue);
    }

    @Test
    @DisplayName("Another user in group of entity's creator gets entity value")
    void testGetEntityValue_anotherUserInCreatorsGroup_returnedEntityValue() {
        final String userName = "AnotherUser";
        final String groupName = "AnotherGroup";
        final List<Permission> permissions = Collections.singletonList(Permission.read);
        final Group anotherGroup = userService.createGroup(groupName, permissions);
        final User anotherUser = userService.createUser(userName, anotherGroup);
        anotherGroup.addUser(anotherUser);
        anotherGroup.addUser(user);

        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String foundValue1 = entityService.getEntityValue(user, entityName);
        final String foundValue2 = entityService.getEntityValue(anotherUser, entityName);

        assertAll(() -> assertEquals(entityValue, foundValue1),
                () -> assertEquals(foundValue1, foundValue2)
        );
    }

    @Test
    void testUpdateEntity() {
        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String updEntityValue = "UpdatedValue";
        final boolean areUpdated = entityService.updateEntity(user, entityName, updEntityValue);
        assertTrue(areUpdated);
    }

    @Test
    void testUpdateEntity_wrongEntityName_entityNotUpdated() {
        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String wrongEntityName = "WrongEntityName";
        final String updEntityValue = "Deutschland";
        final boolean areUpdated = entityService.updateEntity(user, wrongEntityName, updEntityValue);
        assertFalse(areUpdated);
    }

    @Test
    void testUpdateEntity_wrongUser_entityNotUpdated() {
        final String userName = "SomeUser";
        final String groupName = "SomeGroup";
        final List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        final Group someGroup = userService.createGroup(groupName, permissions);
        final User someUser = userService.createUser(userName, someGroup);
        someGroup.addUser(someUser);

        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String updEntityValue = "UpdatedValue";
        final boolean areUpdated = entityService.updateEntity(someUser, entityName, updEntityValue);
        assertFalse(areUpdated);
    }

    @Test
    void testUpdateEntity_haventWritePermissions_entityNotUpdated() {
        final String userName = "SomeUser";
        final String groupName = "SomeGroup";
        final List<Permission> permissions = Collections.singletonList(Permission.read);
        final Group someGroup = userService.createGroup(groupName, permissions);
        final User someUser = userService.createUser(userName, someGroup);
        someGroup.addUser(someUser);
        someGroup.addUser(user);

        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String updEntityValue = "UpdatedValue";
        final boolean areUpdated = entityService.updateEntity(someUser, entityName, updEntityValue);
        assertFalse(areUpdated);
    }

    @Test
    @DisplayName("SomeUser's have write permission for User's entity")
    void testUpdateEntity_otherUserHaveWritePermissions_entityUpdated() {
        final String userName = "SomeUser";
        final String groupName = "SomeGroup";
        final List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        final Group someGroup = userService.createGroup(groupName, permissions);
        final User someUser = userService.createUser(userName, someGroup);
        someGroup.addUser(someUser);
        someGroup.addUser(user);

        final String entityName = "EntityName";
        final String entityValue = "EntityValue";
        entityService.createEntity(user, entityName, entityValue);

        final String updEntityValue = "UpdatedValue";
        final boolean areUpdated = entityService.updateEntity(someUser, entityName, updEntityValue);
        assertTrue(areUpdated);
    }
}
